package com.example.mozakrty

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

fun Context.goTo(dest: AppCompatActivity, key:String="", value:String="")
{
    startActivity(Intent(this,dest::class.java).apply {
        putExtra(key,value)
    })
}
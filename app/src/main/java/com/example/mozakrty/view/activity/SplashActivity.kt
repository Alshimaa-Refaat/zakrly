package com.example.mozakrty.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mozakrty.R
import com.example.mozakrty.view.fragment.SplashFragment

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        supportFragmentManager.takeIf { savedInstanceState == null }
            ?.beginTransaction()
            ?.replace(R.id.container,
                SplashFragment()
            )
            ?.commitNow()
    }
}

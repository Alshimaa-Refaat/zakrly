package com.example.mozakrty.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity

import com.example.mozakrty.R
import com.example.mozakrty.goTo
import com.example.mozakrty.view.activity.HomeActivity
import com.example.mozakrty.view.activity.LanguageActivity
import com.example.mozakrty.view.activity.replaceFragment
import kotlinx.android.synthetic.main.fragment_login.view.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {
    lateinit var root:View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_login, container, false)
        root.tCreateAccount.setOnClickListener { replaceFragment(RegisterFragment()) }
        root.btnLogin.setOnClickListener({activity?.goTo(HomeActivity()) })
        return root
    }



    fun replaceFragment(fragment: Fragment){
        fragmentManager!!.beginTransaction().replace(R.id.loginContainer, fragment)
            .addToBackStack(null).commit()

    }


}

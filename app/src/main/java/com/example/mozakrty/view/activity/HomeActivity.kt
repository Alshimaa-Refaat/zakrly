package com.example.mozakrty.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.mozakrty.R
import com.example.mozakrty.view.fragment.NavigationHomeFragment
import com.example.mozakrty.view.fragment.NavigationLessonFragment
import com.example.mozakrty.view.fragment.NavigationProfileFragment
import com.example.mozakrty.view.fragment.NavigationWalletFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item->
        when(item.itemId)
        {
            R.id.ic_home->
            {

                replaceFragment(NavigationHomeFragment())
                return@OnNavigationItemSelectedListener true
            }

            R.id.ic_lesson->
            {

                replaceFragment(NavigationLessonFragment());
                return@OnNavigationItemSelectedListener true
            }
            R.id.ic_profile->
            {

                replaceFragment(NavigationProfileFragment());
                return@OnNavigationItemSelectedListener true
            }
            R.id.ic_wallet->
            {
                println("wa")
                replaceFragment(NavigationWalletFragment());
                return@OnNavigationItemSelectedListener true
            }


        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        replaceFragment(NavigationHomeFragment())
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.home_container, fragment)
        fragmentTransaction.commit()
    }
}

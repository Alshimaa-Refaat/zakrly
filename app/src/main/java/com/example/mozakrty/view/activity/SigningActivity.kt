package com.example.mozakrty.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.mozakrty.R
import com.example.mozakrty.goTo
import com.example.mozakrty.view.fragment.LoginFragment
import kotlinx.android.synthetic.main.activity_signing.*

class SigningActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signing)
    btnLogin.setOnClickListener {  replaceFragment(LoginFragment()) } }


}
fun AppCompatActivity.replaceFragment(fragment: Fragment){
    val fragmentManager = supportFragmentManager
    val transaction = fragmentManager.beginTransaction()
    transaction.replace(R.id.signing_container,fragment)
    transaction.addToBackStack(null)
    transaction.commit()
}
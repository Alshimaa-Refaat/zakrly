package com.example.mozakrty.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mozakrty.R
import com.example.mozakrty.goTo
import kotlinx.android.synthetic.main.activity_language.*

class LanguageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)
        btnArabic.setOnClickListener {  goTo(SigningActivity()) }
    }
}

package com.example.mozakrty.view.fragment

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.mozakrty.R
import com.example.mozakrty.goTo
import com.example.mozakrty.view.activity.LanguageActivity

/**
 * A simple [Fragment] subclass.
 */
class SplashFragment : Fragment() {
    lateinit var root:View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_splash, container, false)

        Handler().postDelayed(
            {
                activity!!.goTo(LanguageActivity())
                activity!!.finish()
                // replaceFragment(LoginFragment())

            },3000
        )
        return root
    }

}
